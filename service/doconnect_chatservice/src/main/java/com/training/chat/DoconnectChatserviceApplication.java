package com.training.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoconnectChatserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoconnectChatserviceApplication.class, args);
	}

}
